<!--
/**
 * COmanage Registry Kerberos Authenticator Plugin Fields
 *
 */
-->
<?php
  // Determine if fields are editable
  $e = false;
  
  if(($this->action == "add" && $permissions['add']) || ($this->action == "edit" && $permissions['edit']))
    $e = true;
    
  // We shouldn't get here if we don't have at least read permission, but check just in case
  
  if(!$e && !$permissions['view'])
    return false;
  
  print $this->Form->hidden('authenticator_id', array('default' => $vv_authid)) . "\n";

?>
<ul id="<?php print $this->action; ?>_kdc_authenticator" class="fields form-list form-list-admin">
  <li>
    <div class="field-name">
      <div class="field-title"><?php print _txt('pl.kdcauthenticator.realm_name'); ?></div>
      <div class="field-desc"><?php print _txt('pl.kdcauthenticator.realm_name.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('realm_name') : filter_var($kdc_authenticators[0]['KdcAuthenticator']['realm_name'],FILTER_SANITIZE_SPECIAL_CHARS)); ?>
    </div>
  </li>
  <li>
    <div class="field-name">
      <div class="field-title"><?php print _txt('pl.kdcauthenticator.adminkrb_princ'); ?></div>
      <div class="field-desc"><?php print _txt('pl.kdcauthenticator.adminkrb_princ.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('adminkrb_princ') : filter_var($kdc_authenticators[0]['KdcAuthenticator']['adminkrb_princ'],FILTER_SANITIZE_SPECIAL_CHARS)); ?>
    </div>
  </li>
  <li>
    <div class="field-name">
      <div class="field-title"><?php print _txt('pl.kdcauthenticator.adminkrb_key'); ?></div>
      <div class="field-desc"><?php print _txt('pl.kdcauthenticator.adminkrb_key.desc'); ?></div>
    </div>
    <div class="field-info">
      <?php if($e) print $this->Form->input('adminkrb_key'); ?>
    </div>
    </li>
    <?php if($e): ?>
    <li class="fields-submit">
      <div class="field-name">
        <span class="required"><?php print _txt('fd.req'); ?></span>
      </div>
      <div class="field-info">
        <?php print $this->Form->submit($submit_label); ?>
      </div>
    </li>
  <?php endif; ?>
</ul>
