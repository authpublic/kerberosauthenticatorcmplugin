<!--
/**
 * COmanage Registry Kdc Authenticator Plugin Fields
 *
 */
-->
<?php
  // Determine if fields are editable
  $e = false;
  
  if($this->action == "manage" && $permissions['manage'])
    $e = true;

  print $this->Form->hidden('kdc_authenticator_id',
                            array('default' => $vv_authenticator['KdcAuthenticator']['id'])) . "\n";
  print $this->Form->hidden('co_person_id', array('default' => $vv_co_person['CoPerson']['id'])) . "\n";
  
  //print $this->Form->hidden('kdc_type', array('default' => KdcEncodingEnum::SSHA)) . "\n";
?>

<div class="co-info-topbox">
  <i class="material-icons">info</i>
  <?php
    $maxlen = isset($vv_authenticator['KdcAuthenticator']['max_length'])
              ? $vv_authenticator['KdcAuthenticator']['max_length']
              : 64;
    $minlen = isset($vv_authenticator['KdcAuthenticator']['min_length'])
              ? $vv_authenticator['KdcAuthenticator']['min_length']
              : 8;
  
    print _txt('pl.kdcauthenticator.info', array($minlen, $maxlen));
  ?>
</div>

<ul id="<?php print $this->action; ?>_kdc" class="fields form-list form-list-admin">
  <!-- If we are editing our own kdc and one already exists, we must provide it
       (otherwise we're probably an admin -->
  <?php if(!empty($vv_current)
           && ($vv_co_person['CoPerson']['id'] == $this->Session->read('Auth.User.co_person_id'))): ?>
  <li>
    <div class="field-name">
      <div class="field-title">
        <?php print _txt('pl.kdcauthenticator.kdc.current'); ?>
        <span class="required">*</span>
      </div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('passwordc', array('type' => 'password')) : ""); ?>
    </div>
  </li>
  <?php endif; // vv_current ?>
  <li>
    <div class="field-name">
      <div class="field-title">
        <?php print _txt('pl.kdcauthenticator.kdc.new'); ?>
        <span class="required">*</span>
      </div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('password') : ""); ?>
    </div>
  </li>
  <li>
    <div class="field-name">
      <div class="field-title">
        <?php print _txt('pl.kdcauthenticator.kdc.again'); ?>
        <span class="required">*</span>
      </div>
    </div>
    <div class="field-info">
      <?php print ($e ? $this->Form->input('password2', array('type' => 'password')) : ""); ?>
    </div>
  </li>
  <?php if($e): ?>
    <li class="fields-submit">
      <div class="field-name">
        <span class="required"><?php print _txt('fd.req'); ?></span>
      </div>
      <div class="field-info">
        <?php print $this->Form->submit($submit_label); ?>
      </div>
    </li>
  <?php endif; ?>
</ul>
