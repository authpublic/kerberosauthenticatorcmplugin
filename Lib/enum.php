<?php
/**
 * COmanage Registry Password Authenticator Enumerations
 *
 */

class KdcEncodingEnum
{
  const Plain = 'NO';    // Not hashed
}
class KdcAuthKdcSourceEnum
{
  const AutoGenerate = 'AG';
  const External     = 'EX'; // ie: set over API
  const SelfSelect   = 'SL';
}
