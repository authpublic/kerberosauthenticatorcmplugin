<?php
/**
 * COmanage Registry Kerberos Authenticator Plugin Language File
 *
 */ 
global $cm_lang, $cm_texts;

// When localizing, the number in format specifications (eg: %1$s) indicates the argument
// position as passed to _txt.  This can be used to process the arguments in
// a different order than they were passed.

$cm_kdc_authenticator_texts['en_US'] = array(
  // Titles, per-controller
  'ct.kdc_authenticators.1'  => 'Kerberos Authenticator',
  'ct.kdc_authenticators.pl' => 'Kerberos Authenticators',
  
  // Error messages
  'er.kdcauthenticator.current'   => 'Current kdc is required',
  'er.kdcauthenticator.match'     => 'New kdcs do not match',
  'er.kdcauthenticator.len.max'   => 'Password cannot be more than %1$s characters',
  'er.kdcauthenticator.len.min'   => 'Password must be at least %1$s characters',
  
  // Plugin texts
  'pl.kdcauthenticator.hash.crypt'     => 'Store as Crypt',
  'pl.kdcauthenticator.hash.crypt.desc' => 'The kdc will be stored in Crypt format (required by the plugin)',
  'pl.kdcauthenticator.hash.plain'     => 'Store as Plain Text',
  'pl.kdcauthenticator.hash.plain.desc' => 'If enabled, the kdc will be stored unhashed in the database',
  'pl.kdcauthenticator.hash.ssha'      => 'Store as Salted SHA 1',
  'pl.kdcauthenticator.hash.ssha.desc' => 'If enabled, the kdc will be stored in Salted SHA 1 format',
  'pl.kdcauthenticator.info'           => 'Your new kdc must be between %1$s and %2$s characters in length.',
  # Kerberos fields name
  'pl.kdcauthenticator.adminkrb_key'            => 'Admin Keberos Key Path',
  'pl.kdcauthenticator.adminkrb_key.desc'      => 'Keberos server key path eg./etc/krb5kdc/kadm5.keytab',
  'pl.kdcauthenticator.adminkrb_princ'         => 'Admin Kerberos Principal Name',
  'pl.kdcauthenticator.adminkrb_princ.desc'    => 'Kerberos server admin name eg.kadmin/admin',
  'pl.kdcauthenticator.realm_name'         => 'Kerberos Realm Name',
  'pl.kdcauthenticator.realm_name.desc'    => 'Kerberos server realm name eg.LIGO-INDIA.IN',
  

  'pl.kdcauthenticator.mod'            => 'Last changed %1$s UTC',
  'pl.kdcauthenticator.kdc.again' => 'New Password Again',
  'pl.kdcauthenticator.kdc.current' => 'Current Password',
  'pl.kdcauthenticator.kdc.new'   => 'New Password',
  'pl.kdcauthenticator.reset'          => 'Password "%1$s" Reset',
  'pl.kdcauthenticator.saved'          => 'Password "%1$s" Set'
);
