<?php
/**
 * COmanage Registry Kdc Model
 *
 */

class Kdc extends AppModel {
	// Define class name for cake
  public $name = "Kdc";
	
	// Add behaviors
  public $actsAs = array('Containable');
	
	// Association rules from this model to other models
	public $belongsTo = array(
    "KdcAuthenticator.KdcAuthenticator",
    "CoPerson"
  );
	
  // Default display field for cake generated views
  public $displayField = "kdc_type";

  // Validation rules for table elements
  public $validate = array(
    'kdc_authenticator_id' => array(
      'rule' => 'numeric',
      'required' => true,
			'allowEmpty' => false
    ),
    'co_person_id' => array(
      'rule' => 'numeric',
      'required' => true,
      'allowEmpty' => false
    ),
    'kdc_type' => array(
      'rule' => array('inList', 
                      array(
                        KdcEncodingEnum::Plain
                      )),
      'required' => true,
      'allowEmpty' => false
    ),
/*		'kdc_type' => array(
			'rule' => 'notBlank',
			'required' => true,
			'allowEmpty' => false
		),*/
	'password' => array(
			'rule' => 'notBlank',
			'required' => true,
			'allowEmpty' => false
		)
 );
}
