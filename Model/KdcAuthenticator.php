<?php
/**
 * COmanage Registry Kdc Authenticator Model
 *
 */

App::uses("AuthenticatorBackend", "Model");

class KdcAuthenticator extends AuthenticatorBackend {
  // Define class name for cake
  public $name = "KdcAuthenticator";

  // Required by COmanage Plugins
  public $cmPluginType = "authenticator";
	
	// Add behaviors
  public $actsAs = array('Containable');
	
  // Document foreign keys
  public $cmPluginHasMany = array(
    "CoPerson" => array("Kdc")
	);
	
	// Association rules from this model to other models
	public $belongsTo = array(
		"Authenticator"
	);
	
	public $hasMany = array(
		"KdcAuthenticator.Kdc"
	);
	
  // Default display field for cake generated views
  public $displayField = "description";
	
  // Validation rules for table elements
  public $validate = array(
    'authenticator_id' => array(
      'rule' => 'numeric',
      'required' => true,
			'allowEmpty' => false
		),
    'kdc_source' => array(
      'rule' => array('inList', array(KdcAuthKdcSourceEnum::AutoGenerate,
                                      KdcAuthKdcSourceEnum::External,
                                      KdcAuthKdcSourceEnum::SelfSelect)),
      'required' => false,
      'allowEmpty' => true
    ),

		'min_length' => array(
      'rule' => 'numeric',
			'required' => false,
			'allowEmpty' => true
		),
		'max_length' => array(
      'rule' => 'numeric',
			'required' => false,
			'allowEmpty' => true
		)
		);	
	// Does we support multiple authenticators per instantiation?
	public $multiple = false;
  
  /**
   * Expose menu items.
   * 
   * @ return Array with menu location type as key and array of labels, controllers, actions as values.
   */
	
  public function cmPluginMenus() {
  	return array();
  }
	
	/**
   * Obtain current data suitable for passing to manage().
   *
   * @param  integer $id				 Authenticator ID
   * @param  integer $backendId  Authenticator Backend ID
   * @param  integer $coPersonId CO Person ID
   * @return Array As returned by find
   * @throws RuntimeException
	 */
	
	public function current($id, $backendId, $coPersonId) {
		$args = array();
		$args['conditions']['Kdc.kdc_authenticator_id'] = $backendId;
		$args['conditions']['Kdc.co_person_id'] = $coPersonId;
		$args['contain'] = false;
		
		return $this->Kdc->find('all', $args);
	}
	
	/**
	 * Manage Authenticator data, as submitted from the view.
	 *
	 * @param  Array   $data					  Array of Authenticator data submitted from the view
	 * @param  integer $actorCoPersonId Actor CO Person ID
	 * @return string Human readable (localized) result comment
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	
	public function manage($data, $actorCoPersonId) {
		
		// Check that kdcs match
		if($data['Kdc']['password'] != $data['Kdc']['password2']) {
			throw new InvalidArgumentException(_txt('er.kdcauthenticator.match'));
		}
    
    // Make sure we have a CO Person ID to operate over
    if(empty($data['Kdc']['co_person_id'])) {
      throw new InvalidArgumentException(_txt('er.notprov.id', array(_txt('ct.co_people.1'))));
    }
		
		// First see if there are any existing records
		$this->_begin();
		    if($this->pluginCfg['KdcAuthenticator']['kdc_source'] == KdcAuthKdcSourceEnum::SelfSelect) {

		$args = array();
		$args['conditions']['Kdc.kdc_authenticator_id'] = $data['Kdc']['kdc_authenticator_id'];
		$args['conditions']['Kdc.co_person_id'] = $data['Kdc']['co_person_id'];
		$args['conditions']['Kdc.kdc_type'] = KdcEncodingEnum::Plain;
		$args['contain'] = false;
		
		$currec = $this->Kdc->find('first', $args);
		
		if(!empty($currec)
			 && $data['Kdc']['co_person_id'] == $actorCoPersonId) {
			// The current kdc is required
			
			if(!kdc_verify($data['Kdc']['passwordc'], $currec['Kdc']['password'])) {
				throw new InvalidArgumentException(_txt('er.kdcauthenticator.current'));
			}
		}
		    }
    $args = array(
      'Kdc.co_person_id' => $data['Kdc']['co_person_id'],
      'Kdc.kdc_authenticator_id' => $data['Kdc']['kdc_authenticator_id']
    );
    
    $this->Kdc->deleteAll($args);
		
    $pData = array();
          $pData[] = array(
        'kdc_authenticator_id' => $data['Kdc']['kdc_authenticator_id'],
				'co_person_id'							=> $data['Kdc']['co_person_id'],
        'password'									=> $data['Kdc']['password'],
        'kdc_type'							=> KdcEncodingEnum::Plain
      );

		if(!$this->Kdc->saveMany($pData)) {
			$this->_rollback();
			throw new RuntimeException(_txt('er.db.save-a', array('Kdc')));
		}
		
		$comment = _txt('pl.kdcauthenticator.saved',
									  array($this->pluginCfg['Authenticator']['description']));
		
		$this->Authenticator
				 ->Co
				 ->CoPerson
				 ->HistoryRecord->record($data['Kdc']['co_person_id'],
																 null,
																 null,
																 $actorCoPersonId,
																 ActionEnum::AuthenticatorEdited,
																 $comment);
		
		$this->_commit();
	
		return $comment;
	}
	
	/**
	 * Reset Authenticator data for a CO Person.
	 *
	 * @param  integer $coPersonId			CO Person ID
	 * @param  integer $actorCoPersonId Actor CO Person ID
	 * @return boolean true on success
	 */
  
  public function reset($coPersonId, $actorCoPersonId) {
    // Perform the reset. We simply delete any authenticators for the specified CO Person.
		
		$args = array();
		$args['conditions']['Kdc.kdc_authenticator_id'] = $this->pluginCfg['KdcAuthenticator']['id'];
		$args['conditions']['Kdc.co_person_id'] = $coPersonId;
		
		// Note deleteAll will not trigger callbacks by default
		$this->Kdc->deleteAll($args['conditions']);
    
    // And record some history
		
		$comment = _txt('pl.kdcauthenticator.reset',
									  array($this->pluginCfg['Authenticator']['description']));
		
		$this->Authenticator
				 ->Co
				 ->CoPerson
				 ->HistoryRecord->record($coPersonId,
																 null,
																 null,
																 $actorCoPersonId,
																 ActionEnum::AuthenticatorDeleted,
																 $comment);
		
		// We always return true
		return true;
	}
	
	/**
	 * Obtain the current Authenticator status for a CO Person.
	 *
	 * @param  integer $coPersonId			CO Person ID
	 * @return Array Array with values
	 * 							 status: AuthenticatorStatusEnum
	 * 							 comment: Human readable string, visible to the CO Person
	 */
	
	public function status($coPersonId) {
		// Is there a kdc for this person?
		
		$args = array();
		$args['conditions']['Kdc.kdc_authenticator_id'] = $this->pluginCfg['KdcAuthenticator']['id'];
		$args['conditions']['Kdc.co_person_id'] = $coPersonId;
    // We constrain to type CRYPT since we require that type
		$args['conditions']['Kdc.kdc_type'] = KdcEncodingEnum::Plain;
		$args['contain'] = false;
		
		$modtime = $this->Kdc->field('modified', $args['conditions']);
		
		if($modtime) {
			return array(
				'status' => AuthenticatorStatusEnum::Active,
				// Note we don't currently have access to local timezone setting (see OrgIdentity for example)
				'comment' => _txt('pl.kdcauthenticator.mod', array($modtime))
			);
		}
		
		return array(
			'status' => AuthenticatorStatusEnum::NotSet,
			'comment' => _txt('fd.set.not')
		);
	}
}
