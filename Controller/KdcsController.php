<?php
/**
 * COmanage Registry Kdc Controller
 *
 */

App::uses("SAMController", "Controller");

class KdcsController extends SAMController {
  // Class name, used by Cake
  public $name = "Kdc";
  
  /**
   * Authorization for this Controller, called by Auth component
   * - precondition: Session.Auth holds data used for authz decisions
   * - postcondition: $permissions set with calculated permissions
   *
   * @return Array Permissions
   */
  
  function isAuthorized() {
    $roles = $this->Role->calculateCMRoles();
    
    // Construct the permission set for this user, which will also be passed to the view.
    $p = array();
    
    // Determine what operations this user can perform
    
    // Merge in the permissions calculated by our parent
    $p = array_merge($p, $this->calculateParentPermissions($this->Kdc->KdcAuthenticator->multiple));
    
    $this->set('permissions', $p);
    return($p[$this->action]);
  }
}


