<?php
/**
 * COmanage Registry Kdc Authenticator Controller
 *
 */

App::uses("SAuthController", "Controller");

class KdcAuthenticatorsController extends SAuthController {
  // Class name, used by Cake
  public $name = "KdcAuthenticators";
  
  /**
   * Authorization for this Controller, called by Auth component
   * - precondition: Session.Auth holds data used for authz decisions
   * - postcondition: $permissions set with calculated permissions
   *
   * @return Array Permissions
   */
  
  function isAuthorized() {
    $roles = $this->Role->calculateCMRoles();
    
    // Construct the permission set for this user, which will also be passed to the view.
    $p = array();
    
    // Determine what operations this user can perform
    
    // Edit an existing KdcAuthenticator?
    $p['edit'] = ($roles['cmadmin'] || $roles['coadmin']);
    
    // View all existing KdcAuthenticators?
    $p['index'] = ($roles['cmadmin'] || $roles['coadmin']);
    
    // View an existing KdcAuthenticator?
    $p['view'] = ($roles['cmadmin'] || $roles['coadmin']);
    
    $this->set('permissions', $p);
    return($p[$this->action]);
  }
}

